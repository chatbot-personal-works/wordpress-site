
Started Import Process
# System Details: 
Debug Mode 		: Disabled
Operating System 	: WINNT
Software 		: Apache/2.4.18 (Win64) PHP/7.0.4
MySQL version 		: 5.7.11
XML Reader 		: Yes
PHP Version 		: 7.0.4
PHP Max Input Vars 	: 2500
PHP Max Post Size 	: 8M
PHP Extension GD 	: Yes
PHP Max Execution Time 	: 120
Max Upload Size 	: 2 MB
Memory Limit 		: Current memory limit 40M. We recommend setting memory to at least 64M.
Timezone 		: 0

-----

Importing Started! - 12:40:40 UTC
---

WHY IMPORT PROCESS CAN FAIL? READ THIS - 
https://wpastra.com/docs/?p=1314&utm_source=demo-import-panel&utm_campaign=import-error&utm_medium=wp-dashboard

---

Inserted WP Form 35
Imported Customizer Settings {"astra-settings":{"display-site-title":false,"ast-header-retina-logo":"https:\/\/websitedemos.net\/co-working-space-elementor\/wp-content\/uploads\/sites\/65\/2017\/09\/logo@2x-free-img.png","site-content-layout":"boxed-container","header-main-rt-section":"widget","header-main-sep":0,"site-sidebar-layout":"no-sidebar","footer-adv":"layout-4","footer-sml-section-1-credit":"Copyright \u00a9 [current_year] [site_title] | <a href=\"#\">Credits<\/a>","footer-sml-section-2":"custom","footer-sml-section-2-credit":"Powered by [site_title]","footer-sml-divider":0,"footer-sml-layout":"footer-sml-layout-2","link-color":"#4f96ce","text-color":"#000000","footer-adv-wgt-title-color":"#3a3a3a","footer-adv-text-color":"#434343","footer-adv-link-color":"#707070","footer-adv-link-h-color":"#0a0a0a","footer-adv-bg-color":"#f3f3f7","footer-bg-color":"#4f96ce","body-font-family":"'Roboto Slab', serif","body-font-weight":"300","headings-font-family":"'Khand', sans-serif","headings-font-weight":"600","button-color":"#ffffff","button-h-color":"#ffffff","button-bg-color":"#4f96ce","button-bg-h-color":"#31659e","button-radius":4,"button-v-padding":7,"theme-auto-version":"2.0.1","single-page-content-layout":"default","single-post-content-layout":"default","archive-post-content-layout":"default","footer-sml-divider-color":"#fff","single-page-sidebar-layout":"default","single-post-sidebar-layout":"default","archive-post-sidebar-layout":"default","astra-addon-auto-version":"2.0.0","woocommerce-content-layout":"default","woocommerce-sidebar-layout":"default","shop-grids":{"desktop":3,"tablet":2,"mobile":1},"shop-no-of-products":"9","theme-color":"#4f96ce","header-bg-obj":{"background-color":""},"content-bg-obj":{"background-color":"#ffffff"},"above-header-bg-obj":{"background-color":""},"below-header-bg-obj":{"background-color":""},"footer-adv-bg-obj":{"background-color":"#f3f3f7","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"footer-bg-obj":{"background-color":"#4f96ce","background-image":"","background-repeat":"repeat","background-position":"center center","background-size":"auto","background-attachment":"scroll"},"site-layout-outside-bg-obj":{"background-color":""},"header-bg-obj-responsive":{"desktop":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"tablet":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"mobile":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"}},"above-header-bg-obj-responsive":{"desktop":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"tablet":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"mobile":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"}},"below-header-bg-obj-responsive":{"desktop":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"tablet":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"mobile":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"}},"primary-menu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-menu-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-menu-h-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-menu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-menu-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-menu-a-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-h-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"primary-submenu-a-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-text-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-link-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-link-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-bg-color":{"desktop":"","tablet":"","mobile":""},"above-header-menu-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-h-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-active-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-active-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-text-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-hover-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-bg-hover-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-active-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-submenu-active-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-text-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-link-hover-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-link-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-menu-text-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-menu-text-hover-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-menu-bg-hover-color-responsive":{"desktop":"#575757","tablet":"","mobile":""},"below-header-current-menu-text-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"below-header-current-menu-bg-color-responsive":{"desktop":"#575757","tablet":"","mobile":""},"below-header-submenu-text-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-submenu-hover-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-submenu-bg-hover-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-submenu-active-color-responsive":{"desktop":"","tablet":"","mobile":""},"below-header-submenu-active-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"above-header-menu-align":"inline","below-header-menu-align":"inline","primary-menu-bg-obj-responsive":{"desktop":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"tablet":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"mobile":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"}},"above-header-menu-bg-obj-responsive":{"desktop":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"tablet":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"},"mobile":{"background-color":"","background-image":"","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","background-attachment":"scroll"}},"mobile-header-toggle-btn-style":"fill","sticky-header-color-site-title-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-color-site-tagline-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-color-h-site-title-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-bg-color-responsive":{"desktop":"rgba(255,255,255,1)","tablet":"","mobile":""},"sticky-header-menu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-menu-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-menu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-menu-h-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-submenu-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-submenu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-header-submenu-h-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-below-header-bg-color-responsive":{"desktop":"rgba(65,64,66,1)","tablet":"","mobile":""},"sticky-below-header-menu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-below-header-menu-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"sticky-below-header-menu-h-color-responsive":{"desktop":"#ffffff","tablet":"","mobile":""},"sticky-below-header-menu-h-a-bg-color-responsive":{"desktop":"#575757","tablet":"","mobile":""},"sticky-below-header-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-below-header-submenu-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-below-header-submenu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-below-header-submenu-h-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-bg-color-responsive":{"desktop":"rgba(255,255,255,1)","tablet":"","mobile":""},"sticky-above-header-menu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-menu-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-menu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-menu-h-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-submenu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-submenu-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-submenu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"sticky-above-header-submenu-h-a-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-header-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-header-color-site-title-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-header-color-h-site-title-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-menu-bg-color-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-menu-color-responsive":{"desktop":"","tablet":"","mobile":""},"transparent-menu-h-color-responsive":{"desktop":"","tablet":"","mobile":""},"different-sticky-retina-logo":false,"different-transparent-retina-logo":false,"different-retina-logo":"1","include-headings-in-typography":true,"above-header-submenu-border":{"top":"1","bottom":"1","left":"1","right":"1"},"above-header-submenu-item-border":1,"below-header-submenu-border":{"top":"1","bottom":"1","left":"1","right":"1"},"below-header-submenu-item-border":1,"above-nav-menu-pointer-effect":"none","below-nav-menu-pointer-effect":"none","above-header-submenu-border-color":"#eaeaea","below-header-submenu-container-animation":"","above-header-submenu-container-animation":"","primary-submenu-border":{"top":"1","bottom":"1","left":"1","right":"1"},"primary-submenu-item-border":1,"nav-menu-pointer-effect":"none","submenu-below-header":false,"primary-submenu-b-color":"#eaeaea","header-main-submenu-container-animation":"","above-header-submenu-item-b-color":"#eaeaea","primary-submenu-item-b-color":"#eaeaea","body-font-variant":"300","headings-font-variant":"600","transparent-header-enable":0,"font-size-h1":{"desktop":"48","tablet":"","mobile":"","desktop-unit":"px","tablet-unit":"px","mobile-unit":"px"},"font-size-h2":{"desktop":"42","tablet":"","mobile":"","desktop-unit":"px","tablet-unit":"px","mobile-unit":"px"},"font-size-h3":{"desktop":"30","tablet":"","mobile":"","desktop-unit":"px","tablet-unit":"px","mobile-unit":"px"},"font-size-page-title":{"desktop":"30","tablet":"","mobile":"","desktop-unit":"px","tablet-unit":"px","mobile-unit":"px"},"logo-title-inline":0,"wc-header-cart-header-colors":false,"shop-quick-view-stick-cart":true,"_astra_pb_compatibility_completed":true},"custom-css":"\/*\nYou can add your own CSS here.\n\nClick the help icon above to learn more.\n*\/\n"}
Importing from XML https://websitedemos.net/wp-content/uploads/astra-sites/65/wxr.xml
Inserted - Term 2 - {"term_id":2,"name":"Coach","slug":"coach","term_group":0,"term_taxonomy_id":2,"taxonomy":"category","description":"","parent":0,"count":0,"filter":"raw"}
Inserted - Term 3 - {"term_id":3,"name":"Important Links","slug":"important-links","term_group":0,"term_taxonomy_id":3,"taxonomy":"nav_menu","description":"","parent":0,"count":0,"filter":"raw"}
Inserted - Term 4 - {"term_id":4,"name":"Top Nav","slug":"top-nav","term_group":0,"term_taxonomy_id":4,"taxonomy":"nav_menu","description":"","parent":0,"count":0,"filter":"raw"}
Inserted - Term 5 - {"term_id":5,"name":"page","slug":"page","term_group":0,"term_taxonomy_id":5,"taxonomy":"elementor_library_type","description":"","parent":0,"count":0,"filter":"raw"}
Inserted - Term 6 - {"term_id":6,"name":"section","slug":"section","term_group":0,"term_taxonomy_id":6,"taxonomy":"elementor_library_type","description":"","parent":0,"count":0,"filter":"raw"}
Inserted - Post 39 - page - Home
Inserted - Post 40 - page - About Us
Inserted - Post 41 - page - Contact
Inserted - Post 42 - page - Amenities
Inserted - Post 43 - page - Pricing
Inserted - Post 44 - nav_menu_item - 
Inserted - Post 45 - nav_menu_item - 
Inserted - Post 46 - nav_menu_item - 
Inserted - Post 47 - nav_menu_item - 
Inserted - Post 48 - nav_menu_item - 
Inserted - Post 49 - nav_menu_item - Terms and Conditions
Inserted - Post 50 - nav_menu_item - Legal
Inserted - Post 51 - nav_menu_item - Business
Inserted - Post 24 - nav_menu_item - Partners
Inserted - Post 52 - nav_menu_item - Blog
Inserted - Post 53 - attachment - logo
Inserted - Post 54 - attachment - logo 2x
Inserted - Post 55 - attachment - favicon
Inserted - Post 56 - custom_css - astra
Inserted - Post 57 - attachment - home background
Inserted - Post 58 - attachment - about us
Inserted - Post 59 - attachment - 24 hours
Inserted - Post 60 - attachment - conference
Inserted - Post 61 - attachment - fast internet
Inserted - Post 62 - attachment - kitchen
Inserted - Post 63 - attachment - printer
Inserted - Post 64 - attachment - gallery image
Inserted - Post 65 - attachment - gallery image
Inserted - Post 66 - attachment - gallery image
Inserted - Post 67 - attachment - gallery image
Inserted - Post 68 - attachment - about us
Inserted - Post 69 - attachment - logo transparent
Inserted - Post 70 - attachment - analytics
Inserted - Post 71 - attachment - girl working
Inserted - Post 72 - attachment - conference room
Inserted - Post 73 - attachment - founder
Inserted - Post 74 - attachment - manager
Started Import Process
# System Details: 
Debug Mode 		: Disabled
Operating System 	: WINNT
Software 		: Apache/2.4.18 (Win64) PHP/7.0.4
MySQL version 		: 5.7.11
XML Reader 		: Yes
PHP Version 		: 7.0.4
PHP Max Input Vars 	: 2500
PHP Max Post Size 	: 8M
PHP Extension GD 	: Yes
PHP Max Execution Time 	: 120
Max Upload Size 	: 2 MB
Memory Limit 		: Current memory limit 40M. We recommend setting memory to at least 64M.
Timezone 		: 0

-----

Importing Started! - 12:41:22 UTC
---

WHY IMPORT PROCESS CAN FAIL? READ THIS - 
https://wpastra.com/docs/?p=1314&utm_source=demo-import-panel&utm_campaign=import-error&utm_medium=wp-dashboard

---

Inserted - Post 75 - attachment - receptionist
Inserted - Post 76 - attachment - client
Complete 
Batch Process Started!
Astra Sites - Importing Images for Blog name 'University of Springfield' (1)
---- Processing Images from Widgets -----
BATCH - SKIP Image - {from filter} - http://localhost/sandhya/wordpress-new/wordpress/wp-content/uploads/2019/10/bg2.jpg - Filter name `astra_sites_image_importer_skip_image`.
---- Processing WordPress Posts / Pages - for "Gutenberg" ----
Inserted - Post 77 - attachment - client
Inserted - Post 83 - attachment - client
Inserted - Post 85 - attachment - client
Inserted - Post 86 - attachment - client
Inserted - Post 88 - attachment - amanities
Inserted - Post 90 - attachment - work floor
Inserted - Post 92 - attachment - kitchen
Inserted - Post 94 - attachment - conference room
Inserted - Post 95 - elementor_library - Header
Inserted - Post 96 - elementor_library - Section (Use only heading)
Inserted - Post 97 - elementor_library - Content
Inserted - Post 100 - attachment - map marker
Inserted - Post 109 - attachment - Yoast SEO Image
Inserted - Post 215 - post - Becoming a great conversationalist
Inserted - Post 223 - post - Writing is simple when lorem quis bibendum
Inserted - Post 234 - post - 5 Signs of a good speaker
Inserted - Post 239 - post - Become a successful person with gravida nibh
Inserted - Post 244 - post - Achieve your goals in lesser time
Inserted - Post 249 - attachment - success
Inserted - Post 252 - attachment - spreaker
Inserted - Post 262 - attachment - courses-02-free-img
Inserted - Post 266 - page - Blog
Inserted - Post 310 - attachment - blog-01-free-img
Inserted - Post 320 - post - Let me help you urna eu felis dapibus
Inserted - Post 323 - attachment - goals
Inserted - Post 330 - nav_menu_item - 
Inserted - Post 331 - nav_menu_item - 
Complete 
Batch Process Started!
Astra Sites - Importing Images for Blog name 'University of Springfield' (1)
---- Processing MISC ----
Post ID: 331
Post ID: 330
Post ID: 24
Post ID: 51
Post ID: 52
Post ID: 49
Post ID: 50
Post ID: 45
Post ID: 46
Post ID: 47
---- Processing Images from Widgets -----
BATCH - SKIP Image - {from filter} - http://localhost/sandhya/wordpress-new/wordpress/wp-content/uploads/2019/10/bg2.jpg - Filter name `astra_sites_image_importer_skip_image`.
---- Processing WordPress Posts / Pages - for "Gutenberg" ----
---- Processing MISC ----
Post ID: 331
Post ID: 330
Post ID: 24
Post ID: 51
Post ID: 52
Post ID: 49
Post ID: 50
Post ID: 45
Post ID: 46
Post ID: 47
Batch Process Complete!