<?php
/**
 * The template for displaying the footer.
 *
 * @package OceanWP WordPress theme
*template2.innerHTML = `<iframe class="bot" style="display:none;position:fixed;width:300px;height:500px;right:150px;bottom:0;" height="700" width="1750" src="https://console.dialogflow.com/api-client/demo/embedded/d3e326db-3b24-4162-b5c8-4e5467139335" style="display:none"></iframe>`;
*<iframe class="bot" height="700" width="1750" style="position: fixed;width: 300px;bottom: 0;right: 0;height: 500px;z-index: 100;" src="https://console.dialogflow.com/api-client/demo/embedded/0a2e19a5-e538-4a1e-a141-2ffa2764b3a7"></iframe>
 */ ?>

        </main><!-- #main -->



<!--<script src='http://192.168.71.42/sandhya/wordpress-new/wordpress/wp-includes/js/track.js' ></script>-->
<!--    <script>-->
<!--    sanalytics('create', 'user_id', 3000)-->
<!--    </script>-->

<script
    src="http://192.168.71.42/sandhya/wordpress-new/wordpress/wp-includes/js/app.js"
    id="df-btn"
    project="coursebot-awbvse"
    iframeurl="http://61.246.226.211:8080"
    width="320px"
    height="52vh"
    openText="Chat"
    closeText="Close"
  ></script>

  <script>
    document.body.addEventListener("click", function(event) {
      if (event.srcElement.id == "subscribe") {
        // Send a message to the iframe
	console.log("subscribe clicked");
        var iframeEl = document.getElementById("the_iframe");
        var messageButton = document.getElementById("subscribe");
        var email = document.getElementById("email");
        iframeEl.contentWindow.postMessage(email.value, "*");
      }
    });

    // view.popup.watch("visible", function() {
    //   messageButton.addEventListener("click", function(e) {
    //     iframeEl.contentWindow.postMessage(email.value, "*");
    //   });
    // });

    //receive a message back from iframe
    window.addEventListener("message", function(e) {
      if (e.origin !== "http://192.168.71.126:8080") return;
      console.log(e.data);
      if (e.data) {
        window.dfToggle();
      }
    });
  </script>

        <?php do_action( 'ocean_after_main' ); ?>

        <?php do_action( 'ocean_before_footer' ); ?>

        <?php
        // Elementor `footer` location
        if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) { ?>

            <?php do_action( 'ocean_footer' ); ?>
            
        <?php } ?>

        <?php do_action( 'ocean_after_footer' ); ?>
                
    </div><!-- #wrap -->

    <?php do_action( 'ocean_after_wrap' ); ?>

</div><!-- #outer-wrap -->

<?php do_action( 'ocean_after_outer_wrap' ); ?>

<?php
// If is not sticky footer
if ( ! class_exists( 'Ocean_Sticky_Footer' ) ) {
    get_template_part( 'partials/scroll-top' );
} ?>

<?php
// Search overlay style
if ( 'overlay' == oceanwp_menu_search_style() ) {
    get_template_part( 'partials/header/search-overlay' );
} ?>

<?php
// If sidebar mobile menu style
if ( 'sidebar' == oceanwp_mobile_menu_style() ) {
    
    // Mobile panel close button
    if ( get_theme_mod( 'ocean_mobile_menu_close_btn', true ) ) {
        get_template_part( 'partials/mobile/mobile-sidr-close' );
    } ?>

    <?php
    // Mobile Menu (if defined)
    get_template_part( 'partials/mobile/mobile-nav' ); ?>

    <?php
    // Mobile search form
    if ( get_theme_mod( 'ocean_mobile_menu_search', true ) ) {
        get_template_part( 'partials/mobile/mobile-search' );
    }

} ?>

<?php
// If full screen mobile menu style
if ( 'fullscreen' == oceanwp_mobile_menu_style() ) {
    get_template_part( 'partials/mobile/mobile-fullscreen' );
} ?>

<?php wp_footer(); ?>
</body>
</html>


