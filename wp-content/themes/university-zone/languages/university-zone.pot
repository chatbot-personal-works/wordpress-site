# <!=Copyright (C) 2019 Rara Theme
# This file is distributed under the GNU General Public License v2 or later.=!>
msgid ""
msgstr ""
"Project-Id-Version: University Zone 1.0.3\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/university-zone\n"
"POT-Creation-Date: 2019-09-10 05:38:41+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#: footer.php:47
msgid "Copyright &copy;"
msgstr ""

#: footer.php:47
msgid "Y"
msgstr ""

#: footer.php:51
msgid "University Zone | Developed By"
msgstr ""

#. Author of the plugin/theme
msgid "Rara Theme"
msgstr ""

#: footer.php:55
msgid "Powered by %s."
msgstr ""

#: footer.php:55
msgid "https://wordpress.org/"
msgstr ""

#: functions.php:46
msgid "Default Header Image"
msgstr ""

#: functions.php:134
msgid "Gallery Image"
msgstr ""

#: header.php:25
msgid "Skip to content (Press Enter)"
msgstr ""

#: inc/child-functions.php:89
msgid "Header Settings"
msgstr ""

#: inc/child-functions.php:108
msgid "%s You can edit the Button with below settings."
msgstr ""

#: inc/child-functions.php:117 inc/child-functions.php:285
#: inc/child-functions.php:415 inc/parent-functions.php:13
#: inc/parent-functions.php:104 sections/section-slider.php:8
msgid "Apply Now"
msgstr ""

#: inc/child-functions.php:131
msgid "CTA Label"
msgstr ""

#: inc/child-functions.php:148
msgid "CTA URL"
msgstr ""

#: inc/child-functions.php:165
msgid "Open In New Tab"
msgstr ""

#: inc/child-functions.php:186
msgid "%s You can edit the Header Search Settings from here"
msgstr ""

#: inc/child-functions.php:202
msgid "Enable Search Form"
msgstr ""

#: inc/child-functions.php:217
msgid "Banner Section"
msgstr ""

#: inc/child-functions.php:244
msgid "Banner Options"
msgstr ""

#: inc/child-functions.php:245
msgid "Choose banner as static image/video."
msgstr ""

#: inc/child-functions.php:248
msgid "Disable Banner Section"
msgstr ""

#: inc/child-functions.php:249
msgid "Static/Video CTA Banner"
msgstr ""

#: inc/child-functions.php:250
msgid "Post Banner"
msgstr ""

#: inc/child-functions.php:261 inc/child-functions.php:425
#: sections/section-slider.php:7
msgid "Better Education for a Better World"
msgstr ""

#: inc/child-functions.php:276
msgid "Banner Title"
msgstr ""

#: inc/child-functions.php:295
msgid "Link Label"
msgstr ""

#: inc/child-functions.php:313
msgid "Link Url"
msgstr ""

#: inc/child-functions.php:334
msgid "Featured Course Post Five"
msgstr ""

#: inc/child-functions.php:350
msgid "Featured Course Post Six"
msgstr ""

#: inc/child-functions.php:396
msgid " -- Choose -- "
msgstr ""

#: inc/parent-functions.php:197
msgid "Facebook"
msgstr ""

#: inc/parent-functions.php:199
msgid "Twitter"
msgstr ""

#: inc/parent-functions.php:201
msgid "Pinterest"
msgstr ""

#: inc/parent-functions.php:203
msgid "LinkedIn"
msgstr ""

#: inc/parent-functions.php:205
msgid "Instagram"
msgstr ""

#: inc/parent-functions.php:207
msgid "YouTube"
msgstr ""

#: inc/parent-functions.php:209
msgid "OK"
msgstr ""

#: inc/parent-functions.php:211
msgid "VK"
msgstr ""

#: inc/parent-functions.php:213
msgid "Xing"
msgstr ""

#: inc/parent-functions.php:225
msgid "Demo and Documentation"
msgstr ""

#: inc/parent-functions.php:236
msgid "Theme Documentation"
msgstr ""

#: inc/parent-functions.php:236 inc/parent-functions.php:238
#: inc/parent-functions.php:240 inc/parent-functions.php:242
#: inc/parent-functions.php:244
msgid "Click here"
msgstr ""

#: inc/parent-functions.php:238
msgid "Theme Demo"
msgstr ""

#: inc/parent-functions.php:240
msgid "Theme info"
msgstr ""

#: inc/parent-functions.php:242
msgid "Support Ticket"
msgstr ""

#: inc/parent-functions.php:244
msgid "More WordPress Themes"
msgstr ""

#: inc/parent-functions.php:247
msgid "About University Zone"
msgstr ""

#: sections/section-blog.php:13 sections/section-courses.php:51
#: sections/section-slider.php:12
msgid "Read More"
msgstr ""

#: sections/section-blog.php:53 sections/section-blog.php:105
msgid "j M, Y"
msgstr ""

#: sections/section-blog.php:54
msgid "g:i A"
msgstr ""

#: sections/section-choose.php:43
msgid "Learn More"
msgstr ""

#. Theme Name of the plugin/theme
msgid "University Zone"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://rarathemes.com/wordpress-themes/university-zone/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"University Zone is a child theme of Education Zone free WordPress theme. "
"This theme features an elegant, attractive and mobile-friendly design to "
"help you create an appealing website for educational institutions like "
"schools, college, university, LMS, Training Center, Academy, Primary "
"School, High school and Kindergarten. You can create a fully responsive "
"professional website using this theme without writing a single line of code "
"or any technical experience. It is an easy to use theme and comes with "
"multiple features and customization options to help you design your dream "
"website. The theme is SEO friendly along with Schema.org  mark up ready to "
"help your website rank at the top of search engine results. Also, it is "
"performance optimized which will give your website visitors a smooth "
"browsing experience while visiting your website. Designed with visitor "
"engagement in mind, University Zone helps you to easily and intuitively "
"create professional and appealing websites. If your school, college or "
"university needs an online home that is dynamic and multi-functional, this "
"theme is an excellent option to start. Check the demo at "
"https://demo.rarathemes.com/university-zone/, documentation at "
"https://docs.rarathemes.com/docs/university-zone/, and get support at "
"https://rarathemes.com/support-ticket/."
msgstr ""

#. Author URI of the plugin/theme
msgid "https://rarathemes.com/"
msgstr ""