<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'university_schema' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kTkg *4XdRuG%#&$FKUsY<SP`-*vHT>LN/+{4WgLm_{9!jS5)fa$nv@Na_:_hN24' );
define( 'SECURE_AUTH_KEY',  ']:!Qp0KG_u]*@H4:`pq4Hi%4B&K,7[H|DxE[xOoo^;:Yx_P?2H5eh94V9Xu>w&mO' );
define( 'LOGGED_IN_KEY',    'Uq-TPuRGrhJIeDw&_%EhT?+$7J#mEIsRPIK{lPxGU}o;5.]Xd3_;YnHIpr6i&27+' );
define( 'NONCE_KEY',        'UPw+l*smQx}x/g^n7C8?<OU%d#07p;<#+I1Jl%>A.?RB46{3JIXuTwcE!_A2%ctl' );
define( 'AUTH_SALT',        '^1/<c]|R^Rx-O7[?a>^( PB98OJ|g+ztYGDB/&-jnv6)r6<6-2FXyU^;?0)7?SJ$' );
define( 'SECURE_AUTH_SALT', '>f*Vk^I|c.#IMfv9_Pw+RP~>:@OB1l@RiS:+zjL&8k^sg~eD=>-:i)uPM|2KA13A' );
define( 'LOGGED_IN_SALT',   'VQ`PAnq0#w,AbbM;=I7IOHms_m>|F?l^9rj,K@hBxR}q%`L_K~yLK6ZSK5(l81rX' );
define( 'NONCE_SALT',       '0T_[hOhwdUO2uParDw,ee(Bu9q;t&S})JrH&x~xN79Q7a(uUkzlJs .4Jq32$$-+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
