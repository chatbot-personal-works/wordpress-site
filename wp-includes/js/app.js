/**
 * Minified by jsDelivr using Terser v3.14.1.
 * Original file: /gh/mishushakov/df-btn@master/df-btn.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
const wrapper = document.querySelector("#df-btn"),
  config = {
    project: wrapper.getAttribute("project"),
    iframeurl: wrapper.getAttribute("iframeurl"),
    width: wrapper.getAttribute("width"),
    height: wrapper.getAttribute("height"),
    openText: wrapper.getAttribute("openText"),
    closeText: wrapper.getAttribute("closeText"),
  };
if (config.project) {
  const M = document.createElement("style");
  (M.innerHTML = `\n    
    .df-btn {\n        box-shadow: 0 1px 2px 0 rgba(60,64,67,0.302),0 1px 3px 1px rgba(60,64,67,0.149);\n        font-family: 'Google Sans', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;\n        background-color: #fff;\n        border-radius: 24px;\n        cursor: pointer;\n        transition: all .08s linear;\n        position: fixed;\n        bottom: 0px;\n        right: 0px;\n        margin: 16px;\n        display: flex;\n        flex-direction: column;\n        z-index: 999\n    }\n\n    .df-btn-text {\n        min-width: 56px;\n        color: #3c4043;\n        display: inline-flex;\n        align-items: center;\n        font-weight: 500;\n        letter-spacing: .25px;\n        transition: all .08s linear;\n        padding: 0 24px 0 0;\n        font-size: .875rem;\n        height: 48px\n    }\n\n    
    .df-btn-text:before {\n        min-width: 56px;\n        height: 48px;\n        background-position: center;\n        background-repeat: no-repeat;\n        background-size: 24px;\n        
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjVweCIgaGVpZ2h0PSIzMnB4IiB2aWV3Qm94PSIwIDAgMjUgMzIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDU1LjEgKDc4MTM2KSAtIGh0dHBzOi8vc2tldGNoYXBwLmNvbSAtLT4KICAgIDx0aXRsZT5wYXRoLTE8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz4KICAgICAgICA8cGF0aCBkPSJNMjMuNzkzMjMwOCw2LjIyMjc2OTIzIEwxMy41MTM4NDYyLDAuMjkwNDYxNTM4IEMxMi44NDkyMzA4LC0wLjA5MzUzODQ2MTUgMTIuMDIyMTUzOCwtMC4wOTM1Mzg0NjE1IDExLjM1NzUzODUsMC4yOTA0NjE1MzggTDEuMDc4MTUzODUsNi4yMjI3NjkyMyBDMC40MTM1Mzg0NjIsNi42MTE2OTIzMSAwLjAwNDkyMzA3NjkyLDcuMzIwNjE1MzggMC4wMDQ5MjMwNzY5Miw4LjA5MzUzODQ2IEwwLjAwNDkyMzA3NjkyLDE5Ljk2OCBDMC4wMDQ5MjMwNzY5MiwyMC43MzYgMC40MTM1Mzg0NjIsMjEuNDQ0OTIzMSAxLjA3ODE1Mzg1LDIxLjgzMzg0NjIgTDYuMjIyNzY5MjMsMjQuODA3Mzg0NiBMNi4yMjI3NjkyMywzMC44Njc2OTIzIEM2LjIyMjc2OTIzLDMxLjIyNzA3NjkgNi41MTMyMzA3NywzMS41MTI2MTU0IDYuODcyNjE1MzgsMzEuNTEyNjE1NCBDNi45ODU4NDYxNSwzMS41MTI2MTU0IDcuMDk0MTUzODUsMzEuNDgzMDc2OSA3LjE5MjYxNTM4LDMxLjQyNCBMMjMuODA4LDIxLjgzODc2OTIgQzI0LjQ3MjYxNTQsMjEuNDU0NzY5MiAyNC44ODYxNTM4LDIwLjc0NTg0NjIgMjQuODgxMzE2OSwxOS45NzI5MjMxIEwyNC44ODEzMTY5LDguMDkzNTM4NDYgQzI0Ljg3NjMwNzcsNy4zMTU2OTIzMSAyNC40NjI3NjkyLDYuNjA2NzY5MjMgMjMuNzkzMjMwOCw2LjIyMjc2OTIzIFoiIGlkPSJwYXRoLTEiPjwvcGF0aD4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJQYWdlLTEiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJsb2dvIj4KICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2stMiIgZmlsbD0id2hpdGUiPgogICAgICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4KICAgICAgICAgICAgPC9tYXNrPgogICAgICAgICAgICA8dXNlIGlkPSJwYXRoLTEiIGZpbGw9IiMwMDAwMDAiIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+');\n        
    content: ''\n    }\n\n    
    .df-btn:hover {\n        box-shadow: 0 1px 3px 0 rgba(60,64,67,0.302), 0 4px 8px 3px rgba(60,64,67,0.149)\n    }\n\n    .df-btn:not(.df-closed){\n        border-radius: 16px\n    }\n\n    .df-btn:not(.df-closed) > 
    .df-btn-text:before {\n        background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBmaWxsPSJub25lIiBkPSJNMCAwaDI0djI0SDBWMHoiLz48cGF0aCBkPSJNMTguMyA1LjcxYy0uMzktLjM5LTEuMDItLjM5LTEuNDEgMEwxMiAxMC41OSA3LjExIDUuN2MtLjM5LS4zOS0xLjAyLS4zOS0xLjQxIDAtLjM5LjM5LS4zOSAxLjAyIDAgMS40MUwxMC41OSAxMiA1LjcgMTYuODljLS4zOS4zOS0uMzkgMS4wMiAwIDEuNDEuMzkuMzkgMS4wMi4zOSAxLjQxIDBMMTIgMTMuNDFsNC44OSA0Ljg5Yy4zOS4zOSAxLjAyLjM5IDEuNDEgMCAuMzktLjM5LjM5LTEuMDIgMC0xLjQxTDEzLjQxIDEybDQuODktNC44OWMuMzgtLjM4LjM4LTEuMDIgMC0xLjR6Ii8+PC9zdmc+')\n    }\n\n    
    .df-btn-content {\n        display: block;\n        border: 0;\n        height: ${config.height ||
      "500px"};\n        width: ${config.width ||
    "320px"};\n        transition: all .25s ease;\n        float: right;\n        opacity: 1\n    }\n\n    
    .df-btn:not(.df-closed) > .df-btn-content {\n        padding-bottom: 16px;\n    }\n\n    .df-closed > .df-btn-content {\n        width: 0;\n        height: 0;\n        opacity: 0\n    }\n\n    @media screen and (max-width: 720px){\n        .df-btn {\n            border-radius: 28px;\n        }\n\n        
    .df-btn:not(.df-closed) {\n            margin: 0px;\n            border-radius: 0px\n        }\n\n        
    .df-btn:not(.df-closed) > .df-btn-content {\n            width: 100vw;\n            height: calc(100vh - 56px);\n            padding-bottom: 0px\n        }\n\n        
    .df-btn-text {\n            padding: 0;\n            height: 56px;\n            font-size: 0;\n        }\n    }\n\n    
    @media (prefers-color-scheme: dark){\n        .df-btn {\n            background-color: #121212\n        }\n\n        .df-btn-text {\n            color: white\n        }\n\n        .df-btn-text:before {\n            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMjVweCIgaGVpZ2h0PSIzMnB4IiB2aWV3Qm94PSIwIDAgMjUgMzIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDU1LjEgKDc4MTM2KSAtIGh0dHBzOi8vc2tldGNoYXBwLmNvbSAtLT4KICAgIDx0aXRsZT5wYXRoLTE8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz4KICAgICAgICA8cGF0aCBkPSJNMjMuNzkzMjMwOCw2LjIyMjc2OTIzIEwxMy41MTM4NDYyLDAuMjkwNDYxNTM4IEMxMi44NDkyMzA4LC0wLjA5MzUzODQ2MTUgMTIuMDIyMTUzOCwtMC4wOTM1Mzg0NjE1IDExLjM1NzUzODUsMC4yOTA0NjE1MzggTDEuMDc4MTUzODUsNi4yMjI3NjkyMyBDMC40MTM1Mzg0NjIsNi42MTE2OTIzMSAwLjAwNDkyMzA3NjkyLDcuMzIwNjE1MzggMC4wMDQ5MjMwNzY5Miw4LjA5MzUzODQ2IEwwLjAwNDkyMzA3NjkyLDE5Ljk2OCBDMC4wMDQ5MjMwNzY5MiwyMC43MzYgMC40MTM1Mzg0NjIsMjEuNDQ0OTIzMSAxLjA3ODE1Mzg1LDIxLjgzMzg0NjIgTDYuMjIyNzY5MjMsMjQuODA3Mzg0NiBMNi4yMjI3NjkyMywzMC44Njc2OTIzIEM2LjIyMjc2OTIzLDMxLjIyNzA3NjkgNi41MTMyMzA3NywzMS41MTI2MTU0IDYuODcyNjE1MzgsMzEuNTEyNjE1NCBDNi45ODU4NDYxNSwzMS41MTI2MTU0IDcuMDk0MTUzODUsMzEuNDgzMDc2OSA3LjE5MjYxNTM4LDMxLjQyNCBMMjMuODA4LDIxLjgzODc2OTIgQzI0LjQ3MjYxNTQsMjEuNDU0NzY5MiAyNC44ODYxNTM4LDIwLjc0NTg0NjIgMjQuODgxMzE2OSwxOS45NzI5MjMxIEwyNC44ODEzMTY5LDguMDkzNTM4NDYgQzI0Ljg3NjMwNzcsNy4zMTU2OTIzMSAyNC40NjI3NjkyLDYuNjA2NzY5MjMgMjMuNzkzMjMwOCw2LjIyMjc2OTIzIFoiIGlkPSJwYXRoLTEiPjwvcGF0aD4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJQYWdlLTEiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJsb2dvIj4KICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2stMiIgZmlsbD0id2hpdGUiPgogICAgICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4KICAgICAgICAgICAgPC9tYXNrPgogICAgICAgICAgICA8dXNlIGlkPSJwYXRoLTEiIGZpbGw9IiNGRkZGRkYiIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+')\n        
  }\n\n        .df-btn:not(.df-closed) > .df-btn-text:before {\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBmaWxsPSJub25lIiBkPSJNMCAwaDI0djI0SDBWMHoiLz48cGF0aCBmaWxsPSIjRkZGRkZGIiBkPSJNMTguMyA1LjcxYy0uMzktLjM5LTEuMDItLjM5LTEuNDEgMEwxMiAxMC41OSA3LjExIDUuN2MtLjM5LS4zOS0xLjAyLS4zOS0xLjQxIDAtLjM5LjM5LS4zOSAxLjAyIDAgMS40MUwxMC41OSAxMiA1LjcgMTYuODljLS4zOS4zOS0uMzkgMS4wMiAwIDEuNDEuMzkuMzkgMS4wMi4zOSAxLjQxIDBMMTIgMTMuNDFsNC44OSA0Ljg5Yy4zOS4zOSAxLjAyLjM5IDEuNDEgMCAuMzktLjM5LjM5LTEuMDIgMC0xLjQxTDEzLjQxIDEybDQuODktNC44OWMuMzgtLjM4LjM4LTEuMDIgMC0xLjR6Ii8+PC9zdmc+')\n       
 }\n    }`),
    document.head.appendChild(M),
    document.write(
      `\n        <div class="df-btn df-closed" onclick="dfToggle()">\n            <div class="df-btn-text">${config.openText ||
        "Chat"}</div>\n            <iframe class="df-btn-content" id="the_iframe" src="${
        config.iframeurl
      }"></iframe>\n        </div>\n    `
    );
  let I = !1;
  window.dfToggle = () => {
    (document.querySelector(".df-btn").classList = I
      ? "df-btn df-closed"
      : "df-btn"),
      (document.querySelector(".df-btn-text").innerText = I
        ? config.openText || "Chat"
        : config.closeText || "Close"),
      (I = !I);
      console.log("ran");
  };
} else console.warn("Please specify your project ID in attributes!");
//# sourceMappingURL=/sm/a2798d4a9e7706ac2703eacc4a9c4e0ad3050201d2007c37909b4361f3c2ab8d.map
