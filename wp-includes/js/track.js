(function () {
    var Doc = document,
        Win = window,
        analyticsUrl = "http://192.168.71.84:5001/collect",
        perfTiming = Win.performance.timing,
        userId = '',
        domLoaded = 0,
        totalSpent = 0,
        inactiveTime = 0,
        pageEvents = [],
        inactiveTimings = [],
        lastActive = 0,
        fnCall = null,
        fnTime = 0,
        pageId = "",
        uid = "",
        aid = "",
        asid = "",
        pageUrl = "",
        reqQueue = [],
        customData = {},
        inputEvents = ["textarea", "select"]
    ID = function () {
        return Math.random().toString(36).substr(2, 9) + '_' + Math.random().toString(36).substr(2, 9);
    }, buildURLQuery = function (obj) {
        return '?' + Object.entries(obj).map(pair => pair.map(encodeURIComponent).join('=')).join('&');
    }, getOS = function () {
        var userAgent = Win.navigator.userAgent,
            platform = Win.navigator.platform,
            macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
            windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
            iosPlatforms = ['iPhone', 'iPad', 'iPod'],
            os = null;
        if (macosPlatforms.indexOf(platform) !== -1) {
            os = 'Mac OS';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
            os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os = 'Windows';
        } else if (/Android/.test(userAgent)) {
            os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
            os = 'Linux';
        }
        return os;
    }, browserDetails = function () {
        var ua = navigator.userAgent,
            tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
        return M;
    }, randomString = function (len = 12) {
        charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    }, getCookie = function (name) {
        var match = document.cookie.match(RegExp('(?:^|;\\s*)' + name + '=([^;]*)'));
        return match ? match[1] : null;
    }, objectId = function () {
        return hex(Date.now() / 1000) + ' '.repeat(16).replace(/./g, () => hex(Math.random() * 16))
    }, hex = function (value) {
        return Math.floor(value).toString(16)
    }

    function attachEventListener(type, listener) {
        Win.addEventListener(type, listener);
    }

    function attachEventListenerDoc(type, listener) {
        Doc.addEventListener(type, listener);
    }

    function sendPayload(url, data, beacon = true) {
        if (beacon && Win.navigator.sendBeacon) {
            Win.navigator.sendBeacon(url, data);
        }
    }

    function handleVisibilityChange() {
        if (document.hidden) {
            if (domLoaded) {
                totalSpent += (Date.now() - domLoaded);
            } else {
                totalSpent += (Date.now() - perfTiming.domComplete);
            }
            clearTimeout(fnCall);
        } else {
            domLoaded = Date.now();
            if (fnTime) {
                fnCall = setTimeout(scheduleUpload, fnTime);
            }
        }
    }

    function inactiveTimeMeasure() {
        if (lastActive && (Date.now() - lastActive) > 60000) {
            inactiveTime += Date.now() - lastActive;
            var timings = {
                'lastActive': lastActive,
                'resumed': Date.now()
            }
            inactiveTimings.push(timings);
        }
        lastActive = Date.now();
        return inactiveTime;
    }

    function onActiveUse() {
        inactiveTimeMeasure();
    }

    function onOffline() {
        clearTimeout(fnCall);
        localStorage.setItem('analytics', JSON.stringify(pageEvents))
        localStorage.setItem('pageId', pageId)
    }

    function onOnline() {
        var data = localStorage.getItem('analytics');
        if (data) {
            localStorage.setItem('analytics', '');
            scheduleUpload();
        }
    }

    function onClick(event) {
        if ((event.target.localName == 'input' && event.target.type == 'submit') || inputEvents.indexOf(event.target.localName) == -1) {
            var clickDetails = {
                targetId: event.target.id,
                targetText: (event.target.innerText || event.target.value).substr(0, 100),
                targetNode: event.target.localName,
                type: event.type,
                createdAt: Date.now()
            }
            pageEvents.push(clickDetails);
        }
    }

    function onChange(event) {
        if ((event.target.localName == 'input' && event.target.type == 'email') || inputEvents.indexOf(event.target.localName) > -1) {
            var clickDetails = {
                targetId: event.target.id,
                targetText: event.target.value,
                targetNode: event.target.localName,
                type: event.type,
                createdAt: Date.now()
            }
            pageEvents.push(clickDetails);
            if (event.target.type === 'email') {
                uid = event.target.value ? event.target.value : uid;
                var c = userId + "=" + uid + "; path=/;";
                Doc.cookie = c;
            }
        }
    }

    function onLoad() {
        domLoaded = perfTiming.domComplete;
        pageId = objectId();
        uid = getCookie(userId);
        aid = getCookie('aid');
        asid = getCookie('asid');
        pageUrl = Win.location.pathname;
        var loadTime = perfTiming.domContentLoadedEventEnd - perfTiming.navigationStart,
            pageTitle = Doc.title,
            previousPage = Doc.referrer.indexOf(location.protocol + "//" + location.host) === 0 ? Doc.referrer : "",
            source = Doc.referrer.indexOf(location.protocol + "//" + location.host) === 0 ? "" : Doc.referrer,
            browser = browserDetails(),
            isLoggedIn = false,
            clientOs = getOS(),
            clientBrowserName = browser[0],
            clientBrowserVersion = browser[1];
        if (uid != '' && uid != null) {
            isLoggedIn = true
        }
        var body = {
            pageId,
            aid,
            asid,
            uid: uid ? uid : '',
            isLoggedIn,
            clientOs,
            clientBrowserName,
            clientBrowserVersion,
            pageUrl,
            pageTitle,
            pageHeight: Win.screen.height,
            pageWidth: Win.screen.width,
            previousPage,
            source,
            loadTime,
            startTime: domLoaded,
            type: 'pageload',
            createdAt: Date.now(),
            timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone || '',
            timeZoneOffset: new Date().getTimezoneOffset()
        };
        body = {
            ...body,
            ...customData
        };
        var url = analyticsUrl + buildURLQuery(body);
        fetch(url);
        var data = localStorage.getItem('analytics');
        if (data) {
            data = JSON.parse(data);
            var body = {
                pageId: localStorage.getItem('pageId'),
                asid,
                uid,
                pageEvents: data,
                type: 'events',
                createdAt: Date.now()
            };
            body = {
                ...body,
                ...customData
            };
            var options = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(body),
            }
            fetch(analyticsUrl, options).then(() => {
                localStorage.setItem('analytics', '');
                localStorage.setItem('pageId', '')
            });
        }
    }

    function onUnload() {
        var endTime = Date.now(),
            totalInactiveTime = inactiveTimeMeasure();
        clearTimeout(fnCall);
        var body = {
            pageUrl,
            pageId,
            asid,
            uid,
            endTime,
            totalActiveTime: (endTime - perfTiming.domComplete) - totalInactiveTime,
            totalInactiveTime,
            totalTime: endTime - perfTiming.domComplete,
            inactiveTimings,
            pageEvents,
            type: 'pageunload',
            createdAt: Date.now()
        };
        body = {
            ...body,
            ...customData
        };
        if (domLoaded) {
            sendPayload(analyticsUrl, JSON.stringify(body));
        }
    }

    function scheduleUpload() {
        clearTimeout(fnCall);
        if (pageEvents.length) {
            var body = {
                pageUrl,
                pageId,
                asid,
                uid,
                pageEvents,
                type: 'schedule',
                createdAt: Date.now()
            };
            body = {
                ...body,
                ...customData
            };
            var options = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(body),
            }
            var request = {
                options,
                retry: 1,
            }
            reqQueue.push(request);
            pageEvents = [];
            syncEvents();
        } else {
            fnCall = setTimeout(scheduleUpload, fnTime);
        }
    }

    function syncEvents() {
        if (reqQueue.length) {
            var request = reqQueue.shift()
            fetch(analyticsUrl, request.options).then(() => {
                if (reqQueue.length) {
                    syncEvents();
                } else {
                    fnCall = setTimeout(scheduleUpload, fnTime);
                }
            }).catch(() => {
                if (request.retry <= 3) {
                    reqQueue.unshift(request);
                }
                syncEvents();
            });
        } else {
            fnCall = setTimeout(scheduleUpload, fnTime);
        }
    }

    function addListeners() {
        attachEventListener('load', onLoad);
        attachEventListenerDoc('visibilitychange', handleVisibilityChange);
        attachEventListenerDoc('click', onClick);
        attachEventListener('unload', onUnload);
        attachEventListener('keypress', onActiveUse);
        attachEventListener('mousemove', onActiveUse);
        attachEventListener('scroll', onActiveUse);
        attachEventListener('online', onOnline);
        attachEventListener('offline', onOffline);
        attachEventListener('change', onChange);
    }

    function callTimeout(time) {
        clearTimeout(fnCall);
        fnCall = setTimeout(scheduleUpload, time);
    }

    function createCookie() {
        if (!getCookie('aid')) {
            var expires = new Date((new Date).getTime() + 315.36E8).toGMTString()
            var c = "aid = " + ID() + ";" + "expires=" + expires + ";";
            Doc.cookie = c;
        }
        if (!getCookie('asid')) {
            var c = "asid = " + randomString() + ";";
            Doc.cookie = c;
        }
        addListeners()
    }

    function callFunction() {
        var fn = arguments[0];
        if (fn == "create") {
            createCookie();
            userId = arguments[1] ? arguments[1] : '';
        }
        if (arguments[2]) {
            fnTime = arguments[2];
            callTimeout(fnTime);
        }
        if (arguments[3]) {
            addCustomFields(arguments[3]);
        }
    }

    function addCustomFields(data) {
        var keys = data.split(',');
        keys.map(key => {
            var keyData = key.split(':');
            var customKey = keyData[0];
            var customValue = getCookie(keyData[1]);
            if (customValue) {
                customData[customKey] = customValue;
            }
        })
    }
    window['sanalytics'] = callFunction;
})();